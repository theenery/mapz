using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WG
{
    public enum Block : short
    {
        None,
        Stone,
        Dirt,
        Grass,
        Sand,
        CoalOre,
        IronOre,
        DiamondOre,
        Oak,
        OakLeaves,
        Planks,
        Stick,
        WoodPixaxe
    }

    public class Chunk
    {
        public Block[,,] blocks;
        public Chunk()
        {
            blocks = new Block[16, 16, 16];
        }
        public void SetOre(OreVein oreVein, int xPos, int yPos, int zPos)
        {
            for (int dx = 0; dx < oreVein.diameter; dx++)
            {
                for (int dy = 0; dy < oreVein.diameter; dy++)
                {
                    for (int dz = 0; dz < oreVein.diameter; dz++)
                    {
                        if (xPos + dx < 16 && yPos + dy < 16 && zPos + dz < 16)
                        {
                            blocks[xPos + dx, yPos + dy, zPos + dz] = oreVein.block();
                        }
                    }
                }
            }
        }
        public void SetTree(Tree tree, int xPos, int yPos, int zPos)
        {
            for (int x = 0; x < tree.sizeX; x++)
            {
                for (int y = 0; y < tree.sizeY; y++)
                {
                    for (int z = 0; z < tree.sizeZ; z++)
                    {
                        if (xPos + x - tree.sizeX / 2 < 16 && yPos + y < 16 && zPos + z - tree.sizeZ / 2 < 16 && tree.blocks[x, y, z] != Block.None)
                        {
                            this.blocks[xPos + x - tree.sizeX / 2, yPos + y, zPos + z - tree.sizeZ / 2] = tree.blocks[x, y, z];
                        }
                    }
                }
            }
        }
    }

    public class ChunkGenerator
    {
        private static ChunkGenerator instance;
        public static ChunkGenerator getInstance()
        {
            if (instance == null) instance = new ChunkGenerator();
            return instance;
        }
        private ChunkGenerator() { }
        public void MakeChunk(ChunkBuilder builder)
        {
            builder.Reset();
            builder.SetGround();
            builder.SetOres();
            builder.SetPlants();
        }
    }

    public interface ChunkBuilder
    {
        public void Reset();
        public void SetGround();
        public void SetOres();
        public void SetPlants();
        public Chunk GetChunk();
    }

    public class ForestChunkBuilder : ChunkBuilder
    {
        private Chunk chunk = new Chunk();
        private BiomComponentsFactory biomFactory = new ForestComponentsFactory();
        private int grassLayerHeight = 1;
        private int dirtLayerHeight = 3;
        private int stoneLayerHeight = 4;
        public void Reset()
        {
            chunk = new Chunk();
        }
        public void SetGround()
        {
            for (int x = 0; x < 16; x++)
            {
                for (int z = 0; z < 16; z++)
                {
                    for (int y = 0; y < stoneLayerHeight; y++)
                    {
                        chunk.blocks[x, y, z] = Block.Stone;
                    }
                    for (int y = 0; y < dirtLayerHeight; y++)
                    {
                        chunk.blocks[x, y + stoneLayerHeight, z] = Block.Dirt;
                    }
                    for (int y = 0; y < grassLayerHeight; y++)
                    {
                        chunk.blocks[x, y + stoneLayerHeight + dirtLayerHeight, z] = Block.Grass;
                    }
                }
            }
        }
        public void SetOres()
        {
            var random = new System.Random();
            int OreVeinsNumber = random.Next(6, 10);
            for (int i = 0; i < OreVeinsNumber; i++)
            {
                OreVein oreVein = biomFactory.CreateOreVein();
                chunk.SetOre(oreVein, random.Next(16), random.Next(stoneLayerHeight), random.Next(16));
            }
        }
        public void SetPlants()
        {
            var random = new System.Random();
            int treeNumber = random.Next(1, 2);
            for (int i = 0; i < treeNumber; i++)
            {
                Tree tree = biomFactory.CreateTree();
                int xPos = random.Next(5, 12);
                int yPos = stoneLayerHeight + dirtLayerHeight + grassLayerHeight;
                int zPos = random.Next(5, 12);
                chunk.SetTree(tree, xPos, yPos, zPos);
            }
        }
        public Chunk GetChunk()
        {
            return chunk;
        }
    }

    public class DesertChunkBuilder : ChunkBuilder
    {
        private Chunk chunk = new Chunk();
        BiomComponentsFactory biomFactory = new DesertComponentsFactory();
        private int sandLayerHeight = 4;
        private int stoneLayerHeight = 4;
        public void Reset()
        {
            chunk = new Chunk();
        }
        public void SetGround()
        {
            for (int x = 0; x < 16; x++)
            {
                for (int z = 0; z < 16; z++)
                {
                    for (int y = 0; y < stoneLayerHeight; y++)
                    {
                        chunk.blocks[x, y, z] = Block.Stone;
                    }
                    for (int y = 0; y < sandLayerHeight; y++)
                    {
                        chunk.blocks[x, y + stoneLayerHeight, z] = Block.Sand;
                    }
                }
            }
        }
        public void SetOres()
        {
            var random = new System.Random();
            int OreVeinsNumber = random.Next(4, 7);
            for (int i = 0; i < OreVeinsNumber; i++)
            {
                OreVein oreVein = biomFactory.CreateOreVein();
                chunk.SetOre(oreVein, random.Next(16), random.Next(stoneLayerHeight), random.Next(16));
            }
        }
        public void SetPlants()
        {
            var random = new System.Random();
            int treeNumber = random.Next(1, 3);
            for (int i = 0; i < treeNumber; i++)
            {
                Tree tree = biomFactory.CreateTree();
                int xPos = random.Next(4, 13);
                int yPos = stoneLayerHeight + sandLayerHeight;
                int zPos = random.Next(4, 13);
                chunk.SetTree(tree, xPos, yPos, zPos);
            }
        }
        public Chunk GetChunk()
        {
            return chunk;
        }
    }

    public interface BiomComponentsFactory
    {
        public Tree CreateTree();
        public OreVein CreateOreVein();
    }

    public class ForestComponentsFactory : BiomComponentsFactory
    {
        public Tree CreateTree()
        {
            var random = new System.Random();
            ForestTree tree = new ForestTree();
            int stemHeight = random.Next(5, 8);
            int leavesRadius = tree.leavesRadius = random.Next(2, 4);
            int sizeX = tree.sizeX = leavesRadius * 2 + 1;
            int sizeY = tree.sizeY = stemHeight + leavesRadius;
            int sizeZ = tree.sizeZ = leavesRadius * 2 + 1;
            tree.blocks = new Block[sizeX, sizeY, sizeZ];
            Vector3 center = new Vector3(sizeX / 2, stemHeight, sizeZ / 2);
            for (int x = 0; x < sizeX; x++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    for (int z = 0; z < sizeZ; z++)
                    {
                        Vector3 current = new Vector3(x, y, z);
                        if (Vector3.Distance(center, current) < leavesRadius + 1)
                        {
                            tree.blocks[x, y, z] = Block.OakLeaves;
                        }
                    }
                }
            }
            for (int z = 0; z < stemHeight; z++)
            {
                tree.blocks[sizeX / 2, z, sizeZ / 2] = Block.Oak;
            }
            return tree;
        }
        public OreVein CreateOreVein()
        {
            var random = new System.Random();
            int choice = random.Next(10);
            if (choice < 4)
            {
                return new CoalOreVein() { diameter = 3 };
            }
            else
            {
                return new IronOreVein() { diameter = 2 };
            }
        }
    }

    public class DesertComponentsFactory : BiomComponentsFactory
    {
        public Tree CreateTree()
        {
            var random = new System.Random();
            DesertTree tree = new DesertTree();
            tree.sizeX = 1;
            tree.sizeY = random.Next(4, 7);
            tree.sizeZ = 1;
            tree.blocks = new Block[tree.sizeX, tree.sizeY, tree.sizeZ];
            for (int z = 0; z < tree.sizeY; z++)
            {
                tree.blocks[0, z, 0] = Block.Oak;
            }
            return tree;
        }
        public OreVein CreateOreVein()
        {
            var random = new System.Random();
            int choice = random.Next(10);
            if (choice < 8)
            {
                return new CoalOreVein() { diameter = 2 };
            }
            else
            {
                return new DiamondOreVein() { diameter = 2 };
            }
        }
    }

    public abstract class Tree
    {
        public Block[,,] blocks;
        public int sizeX;
        public int sizeY;
        public int sizeZ;
    }

    public class ForestTree : Tree
    {
        public int leavesRadius;
    }

    public class DesertTree : Tree { }

    public abstract class OreVein
    {
        public int diameter;
        public abstract Block block();
    }

    public class CoalOreVein : OreVein
    {
        public override Block block()
        {
            return Block.CoalOre;
        }
    }

    public class IronOreVein : OreVein
    {
        public override Block block()
        {
            return Block.IronOre;
        }
    }

    public class DiamondOreVein : OreVein
    {
        public override Block block()
        {
            return Block.DiamondOre;
        }
    }
}