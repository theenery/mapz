using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Craft : MonoBehaviour, IPointerDownHandler
{
    public ICraftingTree receipt;
    public IInventory inventory;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        (new InventoryProxy((Inventory)inventory)).CraftItem(receipt);
    }
}
