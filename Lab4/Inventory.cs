using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IInventory
{
    bool AddItem(WG.Block id);
    bool RemoveItem(WG.Block id);
    bool HasItem(WG.Block id);
    bool CraftItem(ICraftingTree item);
}

public class Inventory : MonoBehaviour, IInventory
{
    int slotAmount = 9;
    int storageAmount = 36;
    List<ICraftingTree> receipts = new List<ICraftingTree>();
    List<GameObject> crafts = new List<GameObject>();

    public GameObject slot;
    public GameObject item;
    public GameObject craft;
    public GameObject hotbarPanel;
    public GameObject inventoryPanel;
    public GameObject craftingPanel;
    public Sprite[] sprites;
    FirstPersonController fpc;

    public List<WG.Block> items = new List<WG.Block>();
    public List<GameObject> slots = new List<GameObject>();

    void Start()
    {
        for (int i = 0; i < slotAmount; i++)
        {
            items.Add(WG.Block.None);
            slots.Add(Instantiate(slot));
            slots[i].transform.SetParent(hotbarPanel.transform);
            slots[i].GetComponent<RectTransform>().transform.localScale = Vector3.one;
        }
        for (int i = slotAmount; i < storageAmount; i++)
        {
            items.Add(WG.Block.None);
            slots.Add(Instantiate(slot));
            slots[i].transform.SetParent(inventoryPanel.transform);
            slots[i].GetComponent<RectTransform>().transform.localScale = Vector3.one;
        }

        var oakCraft = new SimpleCrafting(WG.Block.Oak);
        var planksCraft = new ComplexCrafting(new ICraftingTree[] { oakCraft }, WG.Block.Planks, 4);
        var stickCraft = new ComplexCrafting(new ICraftingTree[] { planksCraft, planksCraft }, WG.Block.Stick, 4);
        var woodPixaxeCraft = new ComplexCrafting(new ICraftingTree[] { stickCraft, stickCraft, planksCraft, planksCraft, planksCraft }, WG.Block.WoodPixaxe);

        receipts.Add(oakCraft);
        receipts.Add(planksCraft);
        receipts.Add(stickCraft);
        receipts.Add(woodPixaxeCraft);

        for (int i = 0; i < receipts.Count; i++)
        {
            crafts.Add(Instantiate(slot));
            crafts[i].transform.SetParent(craftingPanel.transform);
            crafts[i].GetComponent<RectTransform>().transform.localScale = Vector3.one;
            GameObject itemObj = Instantiate(craft);
            itemObj.GetComponent<Craft>().receipt = receipts[i];
            itemObj.GetComponent<Craft>().inventory = this;
            itemObj.transform.SetParent(crafts[i].transform);
            itemObj.transform.localScale = Vector3.one;
            itemObj.transform.localPosition = new Vector3();
            itemObj.GetComponent<Image>().sprite = sprites[(int)receipts[i].Output];
        }

        fpc = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>();
        ToggleInventory();
        Cursor.visible = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleInventory();
            Cursor.visible = !Cursor.visible;
            Cursor.lockState = Cursor.lockState == CursorLockMode.Locked ? CursorLockMode.None : CursorLockMode.Locked;
            fpc.cameraCanMove = !fpc.cameraCanMove;
        }
    }

    public bool AddItem(WG.Block id)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if(items[i] == WG.Block.None)
            {
                items[i] = id;
                GameObject itemObj = Instantiate(item);
                itemObj.transform.SetParent(slots[i].transform);
                itemObj.transform.localScale = Vector3.one;
                itemObj.transform.localPosition = new Vector3();
                itemObj.GetComponent<Image>().sprite = sprites[(int)id];
                Debug.Log(id.ToString() + " added to " + i.ToString());
                return true;
            }
        }
        return false;
    }

    public bool RemoveItem(WG.Block id)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] == id)
            {
                foreach (Transform child in slots[i].transform)
                {
                    Destroy(child.gameObject);
                }
                items[i] = WG.Block.None;
                Debug.Log(id.ToString() + " removed from " + i.ToString());
                return true;
            }
        }
        return false;
    }

    public bool HasItem(WG.Block id)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] == id)
            {
                return true;
            }
        }
        return false;
    }

    public bool CraftItem(ICraftingTree item)
    {
        return item.Craft(this);
    }

    void ToggleInventory()
    {
        inventoryPanel.SetActive(!inventoryPanel.activeInHierarchy);
        craftingPanel.SetActive(!craftingPanel.activeInHierarchy);
    }
}

public class InventoryProxy : IInventory
{
    Inventory _inventory;
    List<WG.Block> _items;
    public InventoryProxy(Inventory inventory)
    {
        _inventory = inventory;
        _items = new List<WG.Block>(_inventory.items);
    }
    public bool AddItem(WG.Block id)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            if (_items[i] == WG.Block.None)
            {
                _items[i] = id;
                return true;
            }
        }
        return false;
    }
    public bool RemoveItem(WG.Block id)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            if (_items[i] == id)
            {
                _items[i] = WG.Block.None;
                return true;
            }
        }
        return false;
    }
    public bool HasItem(WG.Block id)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            if (_items[i] == id)
            {
                return true;
            }
        }
        return false;
    }
    public bool CraftItem(ICraftingTree item)
    {
        if(item.Craft(this))
        {
            Debug.Log("Craft available");
            _inventory.CraftItem(item);
            return true;
        }
        return false;
    }
}
