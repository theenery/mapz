using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public int sizeX;
    public int sizeZ;

    public GameObject[] blocks;

    void Start()
    {
        var builders = new WG.ChunkBuilder[] { new WG.ForestChunkBuilder(), new WG.DesertChunkBuilder() };
        var worldBuilder = new WorldFacade(builders, blocks);
        worldBuilder.BuildWorld(sizeX, sizeZ);
    }

    void Update()
    {

    }
}

public class WorldFacade
{
    private System.Random _random;
    private WG.ChunkGenerator _chunkGenerator;
    private WG.ChunkBuilder[] _builders;
    private GameObject[] _blocks;

    public WorldFacade(WG.ChunkBuilder[] builders, GameObject[] blocks)
    {
        _random = new System.Random();
        _chunkGenerator = WG.ChunkGenerator.getInstance();
        _builders = builders;
        _blocks = blocks;
    }

    public void BuildWorld(int sizeX, int sizeZ)
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {
                var builder = _builders[_random.Next(_builders.Length)];
                _chunkGenerator.MakeChunk(builder);
                WG.Chunk chunkProto = builder.GetChunk();
                SetChunk(x, z, chunkProto);
            }
        }
    }

    public void SetChunk(int x, int z, WG.Chunk chunkProto)
    {
        GameObject chunk = new GameObject(string.Format("Chunk#{0}#{1}", x, z));
        chunk.transform.position = new Vector3(16 * x, -16, 16 * z);
        chunk.transform.SetParent(GameObject.FindGameObjectWithTag("World").transform);
        for (int bx = 0; bx < 16; bx++)
        {
            for (int by = 0; by < 16; by++)
            {
                for (int bz = 0; bz < 16; bz++)
                {
                    if (chunkProto.blocks[bx, by, bz] != 0)
                    {
                        SetBlock(bx, by, bz, chunk, chunkProto.blocks[bx, by, bz]);
                    }
                }
            }
        }
    }

    public void SetBlock(int x, int y, int z, GameObject chunk, WG.Block blockProto)
    {
        GameObject block = GameObject.Instantiate(_blocks[(int)blockProto], chunk.transform, false);
        block.transform.localPosition = new Vector3(x, y, z);
    }
}