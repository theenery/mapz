﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1b
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    //Task 2
    public class AccessModifiersDerived : Lab1a.AccessModifiers
    {
        public void TestModifiers()
        {
            PublicMethod();
            //PrivateMethod();
            ProtectedMethod();
            //PrivateProtectedMethod();
            //InternalMethod();
            ProtectedInternalMethod();
        }
    }

    public class AccessModifiersNotDerived
    {
        public void TestModifiers()
        {
            var a = new Lab1a.AccessModifiers();
            a.PublicMethod();
            //a.PrivateMethod();
            //a.ProtectedMethod();
            //a.PrivateProtectedMethod();
            //a.InternalMethod();
            //a.ProtectedInternalMethod();
        }
    }

    // Task 3
    public class AccessModifiersWithoutModifierTest
    {
        public void TestModifiers()
        {
            //var Class = new Lab1a.ClassWithoutModifier();
            //Lab1a.StructWithoutModifier Struct;
            //Lab1a.IInterfaceWithoutModifier Interface;
        }
    }
}
