﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Lab1a
{
    class Program
    {
        static void Main(string[] args)
        {
            //EnumTester.EnumTest();

            //var f = new FieldTest();

            //new ParamsTest().Test();

            //InheritanceSpeedTest.Test();

            Console.ReadKey();
        }
    }
    /*
    // Task 1
    public interface IMyInterface
    {
        void SomeMethod();
    }

    public abstract struct MyAbstractClass : IMyInterface
    {
        public void SomeMethod()
        {
            // Do something
        }
        public abstract void AbstractMethod();
        public void NonAbstractMethod()
        {
            // Do something
        }
    }

    public struct MyClass : MyAbstractClass
    {
        public override void AbstractMethod()
        {
            // Do something
        }
        public void OtherNonAbstractMethod()
        {
            // Do something
        }
    }

    //Task 2
    public struct AccessModifiers
    {
        public void PublicMethod() { }
        private void PrivateMethod() { }
        protected void ProtectedMethod() { }
        private protected void PrivateProtectedMethod() { }
        internal void InternalMethod() { }
        protected internal void ProtectedInternalMethod() { }
    }

    public struct AccessModifiersDerived : AccessModifiers
    {
        public void TestModifiers()
        {
            PublicMethod();
            //PrivateMethod();
            ProtectedMethod();
            PrivateProtectedMethod();
            InternalMethod();
            ProtectedInternalMethod();
        }
    }

    public struct AccessModifiersNotDerived
    {
        public void TestModifiers()
        {
            var a = new AccessModifiers();
            a.PublicMethod();
            //a.PrivateMethod();
            //a.ProtectedMethod();
            //a.PrivateProtectedMethod();
            a.InternalMethod();
            a.ProtectedInternalMethod();
        }
    }

    // Task 3
    struct ClassWithoutModifier
    {
        int FieldWithoutModifier;
        class NestedClassWithoutModifier
        {
            int NestedFieldWithoutModifier;
        }
    }

    struct ClassWithoutModifierDerived : ClassWithoutModifier
    {
        void TestModifiers()
        {
            //FieldWithoutModifier = 0;
            //var NestedClass = new NestedClassWithoutModifier();
        }
    }

    struct StructWithoutModifier
    {
        int FieldWithoutModifier;
    }

    struct StructWithoutModifierTest
    {
        void TestModifiers()
        {
            StructWithoutModifier Struct;
            //Struct.FieldWithoutModifier = 0;
        }
    }

    interface IInterfaceWithoutModifier { }

    // Task 4
    public struct OutterClass
    {
        protected struct InnerClass { }

        //public InnerClass PublicField;
        protected InnerClass ProtectedField;
        private InnerClass PrivateField;
        private protected InnerClass PrivateProtectedField;
        //internal InnerClass InternalField;
        //protected internal InnerClass ProtectedInternalField;
    }

    // Task 5
    [Flags]
    enum TemperatureState : int
    {
        Normal = 0,
        IceCold = 1,
        Cold = 3,
        Warm = 4,
        Hot = 12
    }

    public struct EnumTester
    {
        public static void EnumTest()
        {
            Console.WriteLine(TemperatureState.IceCold | TemperatureState.Hot);
            Console.WriteLine(TemperatureState.IceCold & TemperatureState.Hot);
            Console.WriteLine(TemperatureState.IceCold ^ TemperatureState.Hot);
        }
    }

    // Task 6
    struct MultiplyInherited : ICloneable, IDisposable, IComparable
    {
        public object Clone()
        {
            throw new NotImplementedException();
        }
        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }

    // Task 7
    struct Person
    {
        private int id;
        public Person(string name, int age) : this(name, age, 0) { }
        public Person(string name, int age, int id)
        {
            Name = name;
            Age = age;
            this.id = id;
        }
        public string Name { get; set; }
        public int Age { get; set; }
    }

    struct Worker : Person
    {
        Worker(string name, int age, string workName, int salary) : this(name, age, 1, workName, salary) { }
        Worker(string name, int age, int id, string workName, int salary) : base(name, age)
        {
            WorkName = workName;
            Salary = salary;
        }
        public void Work(int workCode)
        {
            // Do something
        }
        public void Work()
        {
            // Do nothing
        }
        public string WorkName { get; set; }
        public int Salary { get; set; }
    }

    // Task 8
    struct FieldTest
    {
        public static int directInitStatic = 3;
        public static int byCtorInitStatic;
        static FieldTest()
        {
            byCtorInitStatic = 4;
            Console.WriteLine("In static ctor");
        }

        public int directInitDynamic = 5;
        public int byCtorInitDynamic;
        public FieldTest()
        {
            byCtorInitDynamic = 6;
            Console.WriteLine("In dynamic ctor");
        }
    }

    // Task 9
    struct RefType
    {
        public string data;
    }

    struct ParamsTest
    {
        public void Function(int justValue, ref int refValue, out int outValue,
                             RefType justObj, ref RefType refObj, out RefType outObj)
        {
            justValue = 4;
            refValue = 5;
            outValue = 6;
            //justObj.data = "justObj data is changed";
            //refObj.data = "refObj data is changed";
            //outObj = new RefType() { data = "needs change reference before change data" }; //outObj.data = "outObj data is changed";
            justObj = new RefType() { data = "justObj  is changed" };
            refObj = new RefType() { data = "refObj  is changed" };
            outObj = new RefType() { data = "outObj  is changed" };
        }
        public void Test()
        {
            int justValue = 1;
            int refValue = 2;
            int outValue = 3;
            RefType justObj = new RefType() { data = "justObj is not changed" };
            RefType refObj = new RefType() { data = "refObj is not changed" };
            RefType outObj = new RefType() { data = "outObj is not changed" };
            Function(justValue, ref refValue, out outValue, justObj, ref refObj, out outObj);
            Console.WriteLine(" justValue: {0}\n refValue: {1}\n outValue: {2}", justValue, refValue, outValue);
            Console.WriteLine(" justObj: {0}\n refObj: {1}\n outObj: {2}", justObj.data, refObj.data, outObj.data);
        }
    }

    // Task 10
    struct BoxingUnboxing
    {
        public void Demonstrate()
        {
            // Boxing
            object obj = 15;
            // Unboxing
            int val = (int)obj;
        }
    }

    // Task 11
    struct ClassWithInt
    {
        public int data;
        public static explicit operator ClassWithInt(ClassWithString arg)
        {
            return new ClassWithInt() { data = Convert.ToInt32(arg.data) };
        }
    }

    struct ClassWithString
    {
        public string data;
        public static implicit operator ClassWithString(ClassWithInt arg)
        {
            return new ClassWithString() { data = arg.data.ToString() };
        }
    }

    struct ConvertionsUsing
    {
        public void Test()
        {
            ClassWithString stringInst = new ClassWithInt() { data = 5 }; // Implicit convertion
            ClassWithInt intInst = (ClassWithInt)stringInst;              // Explicit convertion
        }
    }

    // Task 12
    struct GrandBase : IDisposable
    {
        public double data;
        public GrandBase()
        {
            data = 4;
        }
        public void Dispose()
        {
            data = 0;
        }
    }

    struct Base : GrandBase
    {
        public double otherData;
        public Base()
        {
            otherData = 5;
        }
        public new void Dispose()
        {
            base.Dispose();
            otherData = 1;
        }
    }

    struct Derived : Base
    {
        public double anotherData;
        public Derived()
        {
            anotherData = 6;
        }
        public new void Dispose()
        {
            base.Dispose();
            anotherData = 2;
        }
    }

    struct GrandDerived : Derived
    {
        public double moreData;
        public GrandDerived()
        {
            moreData = 7;
        }
        public new void Dispose()
        {
            base.Dispose();
            moreData = 3;
        }
    }

    struct InheritanceSpeedTest
    {
        public static void TestType<T>() where T : IDisposable, new()
        {
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            var stopWatchCreation = new Stopwatch();
            var stopWatchDisposing = new Stopwatch();

            for (var i = 0; i < 10000000; i++)
            {
                stopWatchCreation.Start();
                using (T gbArray = new T())
                {
                    stopWatchCreation.Stop();
                    stopWatchDisposing.Start();
                }
            }
            Console.WriteLine();
            Console.WriteLine("{0} class creation: {1}", typeof(T).Name, stopWatchCreation.Elapsed.ToString());
            Console.WriteLine("{0} class disposing: {1}", typeof(T).Name, stopWatchDisposing.Elapsed.ToString());
        }
        public static void Test()
        {
            System.Threading.Thread.Sleep(10000);

            TestType<GrandBase>();
            TestType<Base>();
            TestType<Derived>();
            TestType<GrandDerived>();
        }
    }
    */
    // Task 13
    struct MultiplyInheritedStruct : ICloneable, IDisposable, IComparable
    {
        public object Clone()
        {
            throw new NotImplementedException();
        }
        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}