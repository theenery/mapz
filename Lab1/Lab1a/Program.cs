﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Lab1a
{
    class Program
    {
        static void Main(string[] args)
        {
            //EnumTester.EnumTest();

            //var f = new FieldTest();

            //new ParamsTest().Test();

            //InheritanceSpeedTest.Test();

            SpeedTesting.Test();

            Console.ReadKey();
        }
    }

    // Task 1
    public interface IMyInterface
    {
        void SomeMethod();
    }

    public abstract class MyAbstractClass : IMyInterface
    {
        public void SomeMethod()
        {
            // Do something
        }
        public abstract void AbstractMethod();
        public void NonAbstractMethod()
        {
            // Do something
        }
    }

    public class MyClass : MyAbstractClass
    {
        public override void AbstractMethod()
        {
            // Do something
        }
        public void OtherNonAbstractMethod()
        {
            // Do something
        }
    }

    //Task 2
    public class AccessModifiers
    {
        public void PublicMethod() { }
        private void PrivateMethod() { }
        protected void ProtectedMethod() { }
        private protected void PrivateProtectedMethod() { }
        internal void InternalMethod() { }
        protected internal void ProtectedInternalMethod() { }
    }

    public class AccessModifiersDerived : AccessModifiers
    {
        public void TestModifiers()
        {
            PublicMethod();
            //PrivateMethod();
            ProtectedMethod();
            PrivateProtectedMethod();
            InternalMethod();
            ProtectedInternalMethod();
        }
    }

    public class AccessModifiersNotDerived
    {
        public void TestModifiers()
        {
            var a = new AccessModifiers();
            a.PublicMethod();
            //a.PrivateMethod();
            //a.ProtectedMethod();
            //a.PrivateProtectedMethod();
            a.InternalMethod();
            a.ProtectedInternalMethod();
        }
    }

    // Task 3
    class ClassWithoutModifier
    {
        int FieldWithoutModifier;
        class NestedClassWithoutModifier
        {
            int NestedFieldWithoutModifier;
        }
    }

    class ClassWithoutModifierDerived : ClassWithoutModifier
    {
        void TestModifiers()
        {
            //FieldWithoutModifier = 0;
            //var NestedClass = new NestedClassWithoutModifier();
        }
    }

    struct StructWithoutModifier
    {
        int FieldWithoutModifier;
    }

    struct StructWithoutModifierTest
    {
        void TestModifiers()
        {
            StructWithoutModifier Struct;
            //Struct.FieldWithoutModifier = 0;
        }
    }

    interface IInterfaceWithoutModifier { }

    // Task 4
    public class OutterClass
    {
        protected class InnerClass { }

        //public InnerClass PublicField;
        protected InnerClass ProtectedField;
        private InnerClass PrivateField;
        private protected InnerClass PrivateProtectedField;
        //internal InnerClass InternalField;
        //protected internal InnerClass ProtectedInternalField;
    }

    // Task 5
    [Flags]
    enum TemperatureState : int
    {
        Normal = 0,
        IceCold = 1,
        Cold = 3,
        Warm = 4,
        Hot = 12
    }

    public class EnumTester
    {
        public static void EnumTest()
        {
            Console.WriteLine(TemperatureState.IceCold | TemperatureState.Hot);
            Console.WriteLine(TemperatureState.IceCold & TemperatureState.Hot);
            Console.WriteLine(TemperatureState.IceCold ^ TemperatureState.Hot);
        }
    }

    // Task 6
    class MultiplyInherited : ICloneable, IDisposable, IComparable
    {
        public object Clone()
        {
            throw new NotImplementedException();
        }
        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }

    // Task 7
    class Person
    {
        private int id;
        public Person(string name, int age) : this(name, age, 0) { }
        public Person(string name, int age, int id)
        {
            Name = name;
            Age = age;
            this.id = id;
        }
        public string Name { get; set; }
        public int Age { get; set; }
    }

    class Worker : Person
    {
        Worker(string name, int age, string workName, int salary) : this(name, age, 1, workName, salary) { }
        Worker(string name, int age, int id, string workName, int salary) : base(name, age)
        {
            WorkName = workName;
            Salary = salary;
        }
        public void Work(int workCode)
        {
            // Do something
        }
        public void Work()
        {
            // Do nothing
        }
        public string WorkName { get; set; }
        public int Salary { get; set; }
    }

    // Task 8
    class FieldTest
    {
        public static int directInitStatic = 3;
        public static int byCtorInitStatic;
        static FieldTest()
        {
            byCtorInitStatic = 4;
            Console.WriteLine("In static ctor");
        }

        public int directInitDynamic = 5;
        public int byCtorInitDynamic;
        public FieldTest()
        {
            byCtorInitDynamic = 6;
            Console.WriteLine("In dynamic ctor");
        }
    }

    // Task 9
    class RefType
    {
        public string data;
    }

    class ParamsTest
    {
        public void Function(int justValue, ref int refValue, out int outValue,
                             RefType justObj, ref RefType refObj, out RefType outObj)
        {
            justValue = 4;
            refValue = 5;
            outValue = 6;
            //justObj.data = "justObj data is changed";
            //refObj.data = "refObj data is changed";
            //outObj = new RefType() { data = "needs change reference before change data" }; //outObj.data = "outObj data is changed";
            justObj = new RefType() { data = "justObj  is changed" };
            refObj = new RefType() { data = "refObj  is changed" };
            outObj = new RefType() { data = "outObj  is changed" };
        }
        public void Test()
        {
            int justValue = 1;
            int refValue = 2;
            int outValue = 3;
            RefType justObj = new RefType() { data = "justObj is not changed" };
            RefType refObj = new RefType() { data = "refObj is not changed" };
            RefType outObj = new RefType() { data = "outObj is not changed" };
            Function(justValue, ref refValue, out outValue, justObj, ref refObj, out outObj);
            Console.WriteLine(" justValue: {0}\n refValue: {1}\n outValue: {2}", justValue, refValue, outValue);
            Console.WriteLine(" justObj: {0}\n refObj: {1}\n outObj: {2}", justObj.data, refObj.data, outObj.data);
        }
    }

    // Task 10
    class BoxingUnboxing
    {
        public void Demonstrate()
        {
            // Boxing
            object obj = 15;
            // Unboxing
            int val = (int)obj;
        }
    }

    // Task 11
    class ClassWithInt
    {
        public int data;
        public static explicit operator ClassWithInt(ClassWithString arg)
        {
            return new ClassWithInt() { data = Convert.ToInt32(arg.data) };
        }
    }

    class ClassWithString
    {
        public string data;
        public static implicit operator ClassWithString(ClassWithInt arg)
        {
            return new ClassWithString() { data = arg.data.ToString() };
        }
    }

    class ConvertionsUsing
    {
        public void Test()
        {
            ClassWithString stringInst = new ClassWithInt() { data = 5 }; // Implicit convertion
            ClassWithInt intInst = (ClassWithInt)stringInst;              // Explicit convertion
        }
    }

    // Task 12
    class GrandBase : IDisposable
    {
        public double data;
        public GrandBase()
        {
            data = 4;
        }
        public virtual void Dispose()
        {
            data = 0;
        }
    }

    class Base : GrandBase
    {
        public double otherData;
        public Base()
        {
            otherData = 5;
        }
        public override void Dispose()
        {
            base.Dispose();
            otherData = 1;
        }
    }

    class Derived : Base
    {
        public double anotherData;
        public Derived()
        {
            anotherData = 6;
        }
        public override void Dispose()
        {
            base.Dispose();
            anotherData = 2;
        }
    }

    class GrandDerived : Derived
    {
        public double moreData;
        public GrandDerived()
        {
            moreData = 7;
        }
        public override void Dispose()
        {
            base.Dispose();
            moreData = 3;
        }
    }

    class InheritanceSpeedTest
    {
        public static void TestType<T>() where T : IDisposable, new()
        {
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            var stopWatchCreation = new Stopwatch();
            var stopWatchDisposing = new Stopwatch();

            for (var i = 0; i < 10000000; i++)
            {
                stopWatchCreation.Start();
                using (T gbArray = new T())
                {
                    stopWatchCreation.Stop();
                    stopWatchDisposing.Start();
                }
            }
            Console.WriteLine();
            Console.WriteLine("{0} class creation: {1}", typeof(T).Name, stopWatchCreation.Elapsed.ToString());
            Console.WriteLine("{0} class disposing: {1}", typeof(T).Name, stopWatchDisposing.Elapsed.ToString());
        }
        public static void Test()
        {
            System.Threading.Thread.Sleep(10000);

            TestType<GrandBase>();
            TestType<Base>();
            TestType<Derived>();
            TestType<GrandDerived>();
        }
    }

    // Task 14
    class MyObject
    {
        public int SomeData;
        public override bool Equals(object obj)
        {
            if(obj is MyObject && obj != null)
            {
                return this.SomeData == ((MyObject)obj).SomeData;
            }
            return false;
        }
        // Finalize
        ~MyObject()
        {
            // Free some unmanaged resources
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
        public override string ToString()
        {
            return string.Format("[MyObject data: {0}]", SomeData);
        }
    }

    // Task 15
    struct StructExample
    {
        public double Data;
        public void SomeMethod()
        {
            Data = 2;
        }
    }

    class ClassExample
    {
        public double Data;
        public void SomeMethod()
        {
            Data = 3;
        }
    }

    class DerivedClassExample : ClassExample { }

    class SpeedTesting
    {
        public static void Test()
        {
            var stopWatchCreation = new Stopwatch();
            var stopWatchMethod = new Stopwatch();
            var stopWatchDeleting = new Stopwatch();

            int objNumber = 10000000;
            System.Threading.Thread.Sleep(10000);

            stopWatchCreation.Start();
            StructExample[] structs = new StructExample[objNumber];
            for (var i = 0; i < objNumber; i++)
            {
                structs[i] = new StructExample();
            }
            stopWatchCreation.Stop();
            stopWatchMethod.Start();
            for (var i = 0; i < objNumber; i++)
            {
                structs[i].SomeMethod();
            }
            stopWatchMethod.Stop();
            Console.WriteLine();
            Console.WriteLine("Struct creation: {0}", stopWatchCreation.Elapsed.ToString());
            Console.WriteLine("Struct method calling: {0}", stopWatchMethod.Elapsed.ToString());

            stopWatchCreation.Reset();
            stopWatchMethod.Reset();
            stopWatchDeleting.Reset();

            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();

            stopWatchCreation.Start();
            ClassExample[] objs = new ClassExample[objNumber];
            for (var i = 0; i < objNumber; i++)
            {
                objs[i] = new ClassExample();
            }
            stopWatchCreation.Stop();
            stopWatchMethod.Start();
            for (var i = 0; i < objNumber; i++)
            {
                objs[i].SomeMethod();
            }
            stopWatchMethod.Stop();
            Console.WriteLine();
            Console.WriteLine("Class creation: {0}", stopWatchCreation.Elapsed.ToString());
            Console.WriteLine("Class method calling: {0}", stopWatchMethod.Elapsed.ToString());

            stopWatchCreation.Reset();
            stopWatchMethod.Reset();
            stopWatchDeleting.Reset();

            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();

            stopWatchCreation.Start();
            DerivedClassExample[] derObjs = new DerivedClassExample[objNumber];
            for (var i = 0; i < objNumber; i++)
            {
                derObjs[i] = new DerivedClassExample();
            }
            stopWatchCreation.Stop();
            stopWatchMethod.Start();
            for (var i = 0; i < objNumber; i++)
            {
                derObjs[i].SomeMethod();
            }
            stopWatchMethod.Stop();
            Console.WriteLine();
            Console.WriteLine("Derived class creation: {0}", stopWatchCreation.Elapsed.ToString());
            Console.WriteLine("Derived class method calling: {0}", stopWatchMethod.Elapsed.ToString());
        }
    }
}