﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2Definition
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine();
            Array arr = AnonymousTypeDemonstration();
            foreach (var item in arr) Console.WriteLine(item.ToString());

            Console.WriteLine();
            List<Worker> sortedWorkers = SortingWithComparerDemonstration();
            foreach (var item in sortedWorkers)
                Console.WriteLine($"CompanyName: {item.CompanyName}, Age: {item.Age}, Name: {item.Name}");

            Console.WriteLine();
            List<Worker> complexSortedList = AdvansedSorting();
            complexSortedList.ForEach(
                i => Console.WriteLine($"Name: {i.Name}, Age: {i.Age}, CompanyName: {i.CompanyName}"));

            Console.ReadLine();
        }

        public static List<Worker> GetSomeWorkers()
        {
            return new List<Worker>
            {
                new Worker { Name = "Anton", Age = 18, CompanyName = "Epam"},
                new Worker { Name = "Igor", Age = 18, CompanyName = "GlobalLogic"},
                new Worker { Name = "Ivan", Age = 18, CompanyName = "SoftServe"},
                new Worker { Name = "Kolya", Age = 20, CompanyName = "SoftServe"},
                new Worker { Name = "Oleg", Age = 19, CompanyName = "SoftServe"},
                new Worker { Name = "Vasyl", Age = 20, CompanyName = "GlobalLogic"},
                new Worker { Name = "Veronika", Age = 20, CompanyName = "GlobalLogic"},
                new Worker { Name = "Vika", Age = 19, CompanyName = "Epam"},
                new Worker { Name = "Vlad", Age = 20, CompanyName = "SoftServe"},
                new Worker { Name = "Volodya", Age = 18, CompanyName = "Epam"},
            };
        }

        public static List<Company> GetSomeCompanies()
        {
            return new List<Company>
            {
                new Company { Name = "Epam", Profit = 700},
                new Company { Name = "GlobalLogic", Profit = 900},
                new Company { Name = "SoftServe", Profit = 1000},
            };
        }

        // Task 1
        public static List<string> SelectUsing()
        {
            List<Worker> workers = GetSomeWorkers();
            var selected = from person
                           in workers
                           select person.Name + " " + person.Age.ToString();
            return selected.ToList();
        }

        // Task 2
        public static List<Worker> WhereUsing()
        {
            List<Worker> workers = GetSomeWorkers();
            var whered = from person
                         in workers
                         where person.Age == 18 && person.CompanyName == "Epam"
                         select person;
            return whered.ToList();
        }

        // Task 3
        public static List<Worker> ListUsing()
        {
            List<Worker> workers = GetSomeWorkers();
            workers.AddRange(new List<Worker>
            {
                new Worker { Name = "Olya", Age = 19, CompanyName = "Epam" },
                new Worker { Name = "Max", Age = 20, CompanyName = "SoftServe" }
            });
            workers.FindAll(w => w.CompanyName == "SoftServe" && w.Age > 18);
            workers.ForEach(w => w.CompanyName = "GlobalLogic");
            workers.Reverse();
            return workers;
        }

        public static List<Worker> DictionaryUsing()
        {
            List<Worker> workers = GetSomeWorkers();
            List<Company> companies = GetSomeCompanies();
            Dictionary<Company, List<Worker>> dictionary = workers
                .GroupBy(w => w.CompanyName)
                .ToDictionary(k => companies.Single(c => c.Name == k.Key), v => v.ToList());
            List<Worker> list = dictionary[companies[2]];
            return list;
        }

        // Task 4
        public static int GetSalary(this Company company, List<Worker> anyWorkers)
        {
            int myWorkersCount = anyWorkers.Count(w => company.Name == w.CompanyName);
            return (company.Profit / myWorkersCount) / 10;
        }

        // Task 5, 7
        public static Array AnonymousTypeDemonstration()
        {
            List<Worker> workers = GetSomeWorkers();
            var listOfPersons = workers.Select(w => new { Name = w.Name, Age = w.Age });
            return listOfPersons.ToArray();
        }

        // Task 6
        public class WorkerComparer : IComparer<Worker>
        {
            public int Compare(Worker first, Worker second)
            {
                if (first.CompanyName.CompareTo(second.CompanyName) > 0) return 1;
                else if (first.CompanyName.CompareTo(second.CompanyName) < 0) return -1;
                else
                {
                    if (first.Age > second.Age) return 1;
                    else if (first.Age < second.Age) return -1;
                    else
                    {
                        if (first.Name.CompareTo(second.Name) > 0) return 1;
                        else if (first.Name.CompareTo(second.Name) < 0) return -1;
                        else return 0;
                    }
                }
            }
        }

        public static List<Worker> SortingWithComparerDemonstration()
        {
            List<Worker> workers = GetSomeWorkers();
            workers.Sort(new WorkerComparer());
            return workers;
        }

        // Task 7 => go to task 5

        public static Worker[] ListToArray()
        {
            return GetSomeWorkers().ToArray();
        }

        // Task 8
        public static List<Worker> AdvansedSorting()
        {
            List<Worker> workers = GetSomeWorkers();
            Func<Worker, int> func = delegate (Worker w) { return w.Age; };
            List<Worker> complexData = workers
                .ToLookup(func)
                .ToDictionary(k => k.Key, v => v.ToList())
                .Select(l => l.Value)
                .ToList()
                .OrderBy(delegate(List<Worker> l) { return l.Count(); })
                .SelectMany(l => l)
                .ToList();
            return complexData;
        }
    }

    public class Worker
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string CompanyName { get; set; }
    }

    public class Company
    {
        public string Name { get; set; }
        public int Profit { get; set; }
    }
}
