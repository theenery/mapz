﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

using Lab2Definition;

namespace Lab2Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            List<string> expectedResult = new List<string>
            {
                "Anton 18",
                "Igor 18",
                "Ivan 18",
                "Kolya 20",
                "Oleg 19",
                "Vasyl 20",
                "Veronika 20",
                "Vika 19",
                "Vlad 20",
                "Volodya 18",
            };
            List<string> actualResult = Program.SelectUsing();
            Assert.AreEqual(expectedResult.ToString(), actualResult.ToString());
        }

        [TestMethod]
        public void TestMethod2()
        {
            List<Worker> expectedResult = new List<Worker>
            {
                new Worker { Name = "Anton", Age = 18, CompanyName = "Epam"},
                new Worker { Name = "Volodya", Age = 18, CompanyName = "Epam"},
            };
            List<Worker> actualResult = Program.WhereUsing();
            Assert.AreEqual(expectedResult.ToString(), actualResult.ToString());
        }

        [TestMethod]
        public void TestMethod31()
        {
            List<Worker> expectedResult = new List<Worker>
            {
                new Worker { Name = "Max", Age = 20, CompanyName = "GlobalLogic" },
                new Worker { Name = "Vlad", Age = 20, CompanyName = "GlobalLogic"},
                new Worker { Name = "Oleg", Age = 19, CompanyName = "GlobalLogic"},
                new Worker { Name = "Kolya", Age = 20, CompanyName = "GlobalLogic"},
            };
            List<Worker> actualResult = Program.ListUsing();
            Assert.AreEqual(expectedResult.ToString(), actualResult.ToString());
        }

        [TestMethod]
        public void TestMethod32()
        {
            List<Worker> expectedResult = new List<Worker>
            {
                new Worker { Name = "Anton", Age = 18, CompanyName = "Epam"},
                new Worker { Name = "Vika", Age = 19, CompanyName = "Epam"},
                new Worker { Name = "Volodya", Age = 18, CompanyName = "Epam"},
            };
            List<Worker> actualResult = Program.DictionaryUsing();
            Assert.AreEqual(expectedResult.ToString(), actualResult.ToString());
        }

        [TestMethod]
        public void TestMethod4()
        {
            int expectedResult = (900 / 3) / 10;
            int actualResult = Program.GetSomeCompanies()[1].GetSalary(Program.GetSomeWorkers());
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
