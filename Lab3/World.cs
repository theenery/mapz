using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public short sizeX;
    public short sizeZ;

    public GameObject[] blocks;

    void Start()
    {
        var random = new System.Random();
        WG.ChunkGenerator chunkGenerator = WG.ChunkGenerator.getInstance();
        WG.ChunkBuilder[] builders = new WG.ChunkBuilder[] { new WG.ForestChunkBuilder(), new WG.DesertChunkBuilder() };
        for(int x = 0; x < sizeX; x++)
        {
            for(int z = 0; z < sizeZ; z++)
            {
                WG.ChunkBuilder builder = builders[random.Next(builders.Length)];
                chunkGenerator.MakeChunk(builder);
                WG.Chunk chunkProto = builder.GetChunk();
                GameObject chunk = new GameObject(string.Format("Chunk#{0}#{1}", x, z));
                chunk.transform.position = new Vector3(16 * x, -16, 16 * z);
                chunk.transform.SetParent(GameObject.FindGameObjectWithTag("World").transform);
                for(int bx = 0; bx < 16; bx++)
                {
                    for(int by = 0; by < 16; by++)
                    {
                        for (int bz = 0; bz < 16; bz++)
                        {
                            if(chunkProto.blocks[bx, by, bz] != 0)
                            {
                                GameObject block = Instantiate(blocks[(int)chunkProto.blocks[bx, by, bz]], chunk.transform, false);
                                block.transform.localPosition = new Vector3(bx, by, bz);
                            }
                        }
                    }
                }
            }
        }
    }

    void Update()
    {

    }
}