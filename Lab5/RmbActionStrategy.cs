using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRmbActionStrategy
{
	void execute();
}

public class DoNothingStrategy : IRmbActionStrategy
{
	public void execute()
    {
        // Do nothing
    }
}

public class PlaceStrategy : IRmbActionStrategy
{
	public void execute()
    {
        Camera cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, 10))
        {
            Vector3 spawn = Vector3.zero;

            float xDiff = hit.point.x - hit.transform.position.x;
            float yDiff = hit.point.y - hit.transform.position.y;
            float zDiff = hit.point.z - hit.transform.position.z;

            if(Mathf.Abs(yDiff) == 0.5f)
            {
                spawn = hit.transform.position + (Vector3.up * yDiff) * 2;
            }
            else if (Mathf.Abs(xDiff) == 0.5f)
            {
                spawn = hit.transform.position + (Vector3.right * xDiff) * 2;
            }
            else if (Mathf.Abs(zDiff) == 0.5f)
            {
                spawn = hit.transform.position + (Vector3.forward * zDiff) * 2;
            }
            Inventory inv = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
            new WorldFacade().SetBlock(
                (int)spawn.x % 16,
                (int)spawn.y % 16 + 16,
                (int)spawn.z % 16,
                GameObject.Find(string.Format("Chunk#{0}#{1}", (int)(spawn.x / 16), (int)(spawn.z / 16))),
                inv.items[inv.currentSlot]);
            inv.RemoveItem(inv.items[inv.currentSlot]);
        }
    }
}

public class EatStrategy : IRmbActionStrategy
{
    public void execute()
    {
        Inventory inv = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
        inv.RemoveItem(inv.items[inv.currentSlot]);
        // Heal
    }
}