using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICraftingTree
{
    bool Craft(IInventory inventory);
    int Count { get; }
    Item Output { get; }
}

public class SimpleCrafting : ICraftingTree
{
    public int Count { get => 1; }
    public Item Output { get; private set; }
    
    public SimpleCrafting(Item output)
    {
        Output = output;
    }
    public bool Craft(IInventory inventory)
    {
        return inventory.HasItem(Output);
    }
}

public class ComplexCrafting : ICraftingTree
{
    private ICraftingTree[] _input;
    public int Count { get; private set; }
    public Item Output { get; private set; }

    public ComplexCrafting(ICraftingTree[] input, Item output, int count = 1)
    {
        _input = input;
        Output = output;
        Count = count;
    }
    public bool Craft(IInventory inventory)
    {
        foreach (var item in _input)
        {
            if (!inventory.RemoveItem(item.Output))
            {
                if (!item.Craft(inventory)) return false;
                inventory.RemoveItem(item.Output);
            }
        }
        for (var i = 0; i < Count; i++)
        {
            if (!inventory.AddItem(Output)) return false;
        }
        return true;
    }
}