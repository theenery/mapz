using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISelfDestructible
{
	void Die();
}

public class SelfDestruct : ISelfDestructible
{
	private Listener _listener;
	public SelfDestruct(Listener listener)
    {
		_listener = listener;
    }
	public void Die()
    {
		GameObject.Destroy(_listener != null ? _listener.gameObject : null);
    }
}

public class SelfDrop : ISelfDestructible
{
	private Listener _listener;
	public SelfDrop(Listener listener)
	{
		_listener = listener;
	}
	public void Die()
    {
		GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().AddItem(Item.Apple);
		GameObject.Destroy(_listener != null ? _listener.gameObject : null);
	}
}

public class Destructor : MonoBehaviour
{
	private List<ISelfDestructible> _listeners = new List<ISelfDestructible>();

	public void AddListener(ISelfDestructible listener)
    {
		_listeners.Add(listener);
    }

	public void Destruct()
    {
		foreach(var listener in _listeners)
        {
			listener.Die();
        }
    }

	void OnMouseDown()
	{
		Destruct();
	}
}

public class Listener : MonoBehaviour
{
	public ISelfDestructible listener;
}