using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public int sizeX;
    public int sizeZ;

    void Start()
    {
        var builders = new WG.ChunkBuilder[] { new WG.ForestChunkBuilder(), new WG.DesertChunkBuilder() };
        var worldBuilder = new WorldFacade(builders);
        worldBuilder.BuildWorld(sizeX, sizeZ);
    }

    void Update()
    {

    }
}