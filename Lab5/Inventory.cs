using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IInventory
{
    bool AddItem(Item id);
    bool RemoveItem(Item id);
    bool HasItem(Item id);
    bool CraftItem(ICraftingTree item);
}

public class Inventory : MonoBehaviour, IInventory
{
    int slotAmount = 9;
    int storageAmount = 36;
    List<ICraftingTree> receipts = new List<ICraftingTree>();
    List<GameObject> crafts = new List<GameObject>();

    public GameObject slot;
    public GameObject item;
    public GameObject craft;
    public GameObject hotbarPanel;
    public GameObject inventoryPanel;
    public GameObject craftingPanel;
    FirstPersonController fpc;
    ItemsDB db;

    public List<Item> items = new List<Item>();
    public List<GameObject> slots = new List<GameObject>();

    public int currentSlot = 0;
    public IRmbActionStrategy strategy = new DoNothingStrategy();

    void Start()
    {
        fpc = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>();
        db = GameObject.FindGameObjectWithTag("Inventory").GetComponent<ItemsDB>();

        for (int i = 0; i < slotAmount; i++)
        {
            items.Add(Item.None);
            slots.Add(Instantiate(slot));
            slots[i].transform.SetParent(hotbarPanel.transform);
            slots[i].GetComponent<RectTransform>().transform.localScale = Vector3.one;
        }
        for (int i = slotAmount; i < storageAmount; i++)
        {
            items.Add(Item.None);
            slots.Add(Instantiate(slot));
            slots[i].transform.SetParent(inventoryPanel.transform);
            slots[i].GetComponent<RectTransform>().transform.localScale = Vector3.one;
        }

        var oakCraft = new SimpleCrafting(Item.Oak);
        var planksCraft = new ComplexCrafting(new ICraftingTree[] { oakCraft }, Item.Planks, 4);
        var stickCraft = new ComplexCrafting(new ICraftingTree[] { planksCraft, planksCraft }, Item.Stick, 4);
        var woodPixaxeCraft = new ComplexCrafting(new ICraftingTree[] { stickCraft, stickCraft, planksCraft, planksCraft, planksCraft }, Item.WoodPixaxe);

        receipts.Add(oakCraft);
        receipts.Add(planksCraft);
        receipts.Add(stickCraft);
        receipts.Add(woodPixaxeCraft);

        for (int i = 0; i < receipts.Count; i++)
        {
            crafts.Add(Instantiate(slot));
            crafts[i].transform.SetParent(craftingPanel.transform);
            crafts[i].GetComponent<RectTransform>().transform.localScale = Vector3.one;
            GameObject itemObj = Instantiate(craft);
            itemObj.GetComponent<Craft>().receipt = receipts[i];
            itemObj.GetComponent<Craft>().inventory = this;
            itemObj.transform.SetParent(crafts[i].transform);
            itemObj.transform.localScale = Vector3.one;
            itemObj.transform.localPosition = new Vector3();
            itemObj.GetComponent<Image>().sprite = db.GetItem(receipts[i].Output).Sprite;
        }

        ToggleInventory();
        Cursor.visible = false;

        slots[currentSlot].GetComponent<Image>().color = Color.white;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleInventory();
            Cursor.visible = !Cursor.visible;
            Cursor.lockState = Cursor.lockState == CursorLockMode.Locked ? CursorLockMode.None : CursorLockMode.Locked;
            fpc.cameraCanMove = !fpc.cameraCanMove;
        }

        currentSlot -= (int)(Input.GetAxis("Mouse ScrollWheel") * 10);

            if (currentSlot < 0)
        {
            currentSlot = 8;
        }
        else if (currentSlot > 8)
        {
            currentSlot = 0;
        }

        for (int i = 0; i < slotAmount; i++)
        {
            if (i == currentSlot)
            {
                slots[i].GetComponent<Image>().color = Color.white;
                ItemData itemData = db.GetItem(slots[i].GetComponent<InventoryItem>().item);
                if (itemData.Action == RmbAction.None)
                {
                    strategy = new DoNothingStrategy();
                }
                else if (itemData.Action == RmbAction.Place)
                {
                    strategy = new PlaceStrategy();
                }
                else if (itemData.Action == RmbAction.Eat)
                {
                    strategy = new EatStrategy();
                }
            }
            else
            {
                slots[i].GetComponent<Image>().color = slot.GetComponent<Image>().color;
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            strategy.execute();
        }
    }

    public bool AddItem(Item id)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if(items[i] == Item.None)
            {
                var inventoryItem = slots[i].GetComponent<InventoryItem>();
                inventoryItem.item = id;
                inventoryItem.count = 1;
                items[i] = id;
                GameObject itemObj = Instantiate(item);
                itemObj.transform.SetParent(slots[i].transform);
                itemObj.transform.localScale = Vector3.one;
                itemObj.transform.localPosition = new Vector3();
                itemObj.transform.GetChild(0).GetComponent<Text>().text = inventoryItem.count.ToString();
                itemObj.GetComponent<Image>().sprite = db.GetItem(id).Sprite;
                Debug.Log(id.ToString() + " added to " + i.ToString() + ", count " + inventoryItem.count.ToString());
                return true;
            }
            else if(items[i] == id && slots[i].GetComponent<InventoryItem>().count != (int)db.GetItem(id).StackSize)
            {
                var inventoryItem = slots[i].GetComponent<InventoryItem>();
                inventoryItem.count++;
                slots[i].transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = inventoryItem.count.ToString();
                Debug.Log(id.ToString() + " added to " + i.ToString() + ", count " + inventoryItem.count.ToString());
                return true;
            }
        }
        return false;
    }

    public bool RemoveItem(Item id)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] == id)
            {
                var inventoryItem = slots[i].GetComponent<InventoryItem>();
                inventoryItem.count--;
                slots[i].transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = inventoryItem.count.ToString();
                if (inventoryItem.count == 0)
                {
                    foreach (Transform child in slots[i].transform)
                    {
                        DestroyImmediate(child.gameObject);
                    }
                    items[i] = Item.None;
                    inventoryItem.item = Item.None;
                }
                Debug.Log(id.ToString() + " removed from " + i.ToString() + ", count " + inventoryItem.count.ToString());
                return true;
            }
        }
        return false;
    }

    public bool HasItem(Item id)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] == id)
            {
                return true;
            }
        }
        return false;
    }

    public bool CraftItem(ICraftingTree item)
    {
        return item.Craft(this);
    }

    void ToggleInventory()
    {
        inventoryPanel.SetActive(!inventoryPanel.activeInHierarchy);
        craftingPanel.SetActive(!craftingPanel.activeInHierarchy);
    }
}

public class InventoryProxy : IInventory
{
    Inventory _inventory;
    List<Item> _items;
    List<int> _counts;
    public InventoryProxy(Inventory inventory)
    {
        _inventory = inventory;
        _items = new List<Item>(_inventory.items);
        _counts = _inventory.slots.Select(slot => slot.GetComponent<InventoryItem>().count).ToList();
    }
    public bool AddItem(Item id)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            if (_items[i] == Item.None)
            {
                _items[i] = id;
                _counts[i] = 1;
                return true;
            }
            else if (_items[i] == id)
            {
                _counts[i]++;
                return true;
            }
        }
        return false;
    }
    public bool RemoveItem(Item id)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            if (_items[i] == id)
            {
                _counts[i]--;
                if (_counts[i] == 0)
                {
                    _items[i] = Item.None;
                }
                return true;
            }
        }
        return false;
    }
    public bool HasItem(Item id)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            if (_items[i] == id)
            {
                return true;
            }
        }
        return false;
    }
    public bool CraftItem(ICraftingTree item)
    {
        if(item.Craft(this))
        {
            Debug.Log("Craft available");
            _inventory.CraftItem(item);
            return true;
        }
        return false;
    }
}
