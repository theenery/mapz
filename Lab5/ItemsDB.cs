using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Item : short
{
    None,
    Stone,
    Dirt,
    Grass,
    Sand,
    CoalOre,
    IronOre,
    DiamondOre,
    Oak,
    OakLeaves,
    Planks,
    Stick,
    WoodPixaxe,
    Apple
}

public enum RmbAction : short
{
    None,
    Place,
    Eat
}

public enum StackSize : short
{
    One = 1,
    Small = 16,
    Common = 64
}

[System.Serializable]
public class ItemData
{
    public Item Id;
    public RmbAction Action;
    public StackSize StackSize;
    public string Name;
    public Sprite Sprite;
    public GameObject Block;
    public ItemData(Item id, RmbAction action, StackSize stackSize, string name)
    {
        Id = id;
        Action = action;
        StackSize = stackSize;
        Name = name;
        Sprite = Resources.Load<Sprite>("Sprites/" + Name);
        Block = Action == RmbAction.Place ? Resources.Load<GameObject>("Blocks/" + Name + "Block") : null;
    }
}

public class ItemsDB : MonoBehaviour
{

    public List<ItemData> items = new List<ItemData>();

    void Start()
    {
        items.Add(new ItemData(Item.None, RmbAction.None, StackSize.One, "None"));
        items.Add(new ItemData(Item.Stone, RmbAction.Place, StackSize.Common, "Stone"));
        items.Add(new ItemData(Item.Dirt, RmbAction.Place, StackSize.Common, "Dirt"));
        items.Add(new ItemData(Item.Grass, RmbAction.Place, StackSize.Common, "Grass"));
        items.Add(new ItemData(Item.Sand, RmbAction.Place, StackSize.Common, "Sand"));
        items.Add(new ItemData(Item.CoalOre, RmbAction.Place, StackSize.Common, "CoalOre"));
        items.Add(new ItemData(Item.IronOre, RmbAction.Place, StackSize.Common, "IronOre"));
        items.Add(new ItemData(Item.DiamondOre, RmbAction.Place, StackSize.Common, "DiamondOre"));
        items.Add(new ItemData(Item.Oak, RmbAction.Place, StackSize.Common, "Oak"));
        items.Add(new ItemData(Item.OakLeaves, RmbAction.Place, StackSize.Common, "OakLeaves"));
        items.Add(new ItemData(Item.Planks, RmbAction.Place, StackSize.Common, "Planks"));
        items.Add(new ItemData(Item.Stick, RmbAction.None, StackSize.Common, "Stick"));
        items.Add(new ItemData(Item.WoodPixaxe, RmbAction.None, StackSize.One, "WoodPixaxe"));
        items.Add(new ItemData(Item.Apple, RmbAction.Eat, StackSize.Small, "Apple"));
    }

    void Update()
    {

    }

    public ItemData GetItem(Item id)
    {
        return items.Find((it) => { return it.Id == id; });
    }
}
