using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public Item id = Item.None;
    Inventory inv;

    void Start()
    {
        inv = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
    }

    void Update()
    {
        
    }

    void OnMouseDown()
    {
        inv.AddItem(id);
        Destroy(this.gameObject);
    }
}
