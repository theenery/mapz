using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPoint
{
    private Vector3 _point;

    public TeleportPoint(Vector3 point)
    {
        _point = point;
    }

    public Vector3 GetPoint()
    {
        return _point;
    }
}

public class TeleportManager
{
    private GameObject player;

    public TeleportManager()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public TeleportPoint SavePoint()
    {
        return new TeleportPoint(player.transform.position);
    }

    public void Teleport(TeleportPoint destination)
    {
        player.transform.position = destination.GetPoint();
    }
}

public class TeleportTaker : MonoBehaviour
{
    private TeleportPoint _savedPoint;
    private TeleportManager _manager;

    private void Start()
    {
        _savedPoint = new TeleportPoint(new Vector3());
        _manager = new TeleportManager();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            _savedPoint = _manager.SavePoint();
        }
        else if (Input.GetKeyDown(KeyCode.T))
        {
            _manager.Teleport(_savedPoint);
        }
    }
}