using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldFacade
{
    private System.Random _random;
    private WG.ChunkGenerator _chunkGenerator;
    private WG.ChunkBuilder[] _builders;
    private ItemsDB _db;

    public WorldFacade(WG.ChunkBuilder[] builders = null)
    {
        _random = new System.Random();
        _chunkGenerator = WG.ChunkGenerator.getInstance();
        _builders = builders;
        _db = GameObject.FindGameObjectWithTag("Inventory").GetComponent<ItemsDB>();
    }

    public void BuildWorld(int sizeX, int sizeZ)
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {
                var builder = _builders[_random.Next(_builders.Length)];
                _chunkGenerator.MakeChunk(builder);
                WG.Chunk chunkProto = builder.GetChunk();
                SetChunk(x, z, chunkProto);
            }
        }
    }

    public void SetChunk(int x, int z, WG.Chunk chunkProto)
    {
        GameObject chunk = new GameObject(string.Format("Chunk#{0}#{1}", x, z));
        chunk.transform.position = new Vector3(16 * x, -16, 16 * z);
        chunk.transform.SetParent(GameObject.FindGameObjectWithTag("World").transform);
        for (int bx = 0; bx < 16; bx++)
        {
            for (int by = 0; by < 16; by++)
            {
                for (int bz = 0; bz < 16; bz++)
                {
                    if (chunkProto.blocks[bx, by, bz] != 0)
                    {
                        SetBlock(bx, by, bz, chunk, chunkProto.blocks[bx, by, bz]);
                    }
                }
            }
        }
        foreach(var tree in chunkProto.trees)
        {
            List<Destructor> destructors = new List<Destructor>();
            for (int by = 0; by < tree.sizeY; by++)
            {
                for (int bx = 0; bx < tree.sizeX; bx++)
                {
                    for (int bz = 0; bz < tree.sizeZ; bz++)
                    {
                        if (bx + tree.x < 16 && by + tree.y < 16 && bz + tree.z < 16 && tree.blocks[bx, by, bz] != Item.None)
                        {
                            GameObject block = GameObject.Instantiate(_db.GetItem(tree.blocks[bx, by, bz]).Block, chunk.transform, false);
                            block.transform.localPosition = new Vector3(bx + tree.x, by + tree.y, bz + tree.z);
                            if (tree.blocks[bx, by, bz] == Item.Oak)
                            {
                                Destructor destructor = block.AddComponent<Destructor>();
                                destructors.Add(destructor);
                            }
                            else if (tree.blocks[bx, by, bz] == Item.OakLeaves)
                            {
                                System.Random random = new System.Random();
                                Listener listener = block.AddComponent<Listener>();
                                if (random.Next(10) < 1)
                                {
                                    listener.listener = new SelfDrop(listener);
                                }
                                else
                                {
                                    listener.listener = new SelfDestruct(listener);
                                }
                                destructors[random.Next(destructors.Count)].AddListener(listener.listener);
                            }
                        }
                    }
                }
            }
        }
    }

    public void SetBlock(int x, int y, int z, GameObject chunk, Item blockProto)
    {
        GameObject block = GameObject.Instantiate(_db.GetItem(blockProto).Block, chunk.transform, false);
        block.transform.localPosition = new Vector3(x, y, z);
    }
}

namespace WG
{
    public class Chunk
    {
        public Item[,,] blocks;
        public List<Tree> trees = new List<Tree>();
        public Chunk()
        {
            blocks = new Item[16, 16, 16];
        }
        public void SetOre(OreVein oreVein, int xPos, int yPos, int zPos)
        {
            for (int dx = 0; dx < oreVein.diameter; dx++)
            {
                for (int dy = 0; dy < oreVein.diameter; dy++)
                {
                    for (int dz = 0; dz < oreVein.diameter; dz++)
                    {
                        if (xPos + dx < 16 && yPos + dy < 16 && zPos + dz < 16)
                        {
                            blocks[xPos + dx, yPos + dy, zPos + dz] = oreVein.block();
                        }
                    }
                }
            }
        }
        public void SetTree(Tree tree, int xPos, int yPos, int zPos)
        {
            tree.x = xPos - tree.sizeX / 2;
            tree.y = yPos;
            tree.z = zPos - tree.sizeZ / 2;
            trees.Add(tree);
        }
    }

    public class ChunkGenerator
    {
        private static ChunkGenerator instance;
        public static ChunkGenerator getInstance()
        {
            if (instance == null) instance = new ChunkGenerator();
            return instance;
        }
        private ChunkGenerator() { }
        public void MakeChunk(ChunkBuilder builder)
        {
            builder.Reset();
            builder.SetGround();
            builder.SetOres();
            builder.SetPlants();
        }
    }

    public interface ChunkBuilder
    {
        public void Reset();
        public void SetGround();
        public void SetOres();
        public void SetPlants();
        public Chunk GetChunk();
    }

    public class ForestChunkBuilder : ChunkBuilder
    {
        private Chunk chunk = new Chunk();
        private BiomComponentsFactory biomFactory = new ForestComponentsFactory();
        private int grassLayerHeight = 1;
        private int dirtLayerHeight = 3;
        private int stoneLayerHeight = 4;
        public void Reset()
        {
            chunk = new Chunk();
        }
        public void SetGround()
        {
            for (int x = 0; x < 16; x++)
            {
                for (int z = 0; z < 16; z++)
                {
                    for (int y = 0; y < stoneLayerHeight; y++)
                    {
                        chunk.blocks[x, y, z] = Item.Stone;
                    }
                    for (int y = 0; y < dirtLayerHeight; y++)
                    {
                        chunk.blocks[x, y + stoneLayerHeight, z] = Item.Dirt;
                    }
                    for (int y = 0; y < grassLayerHeight; y++)
                    {
                        chunk.blocks[x, y + stoneLayerHeight + dirtLayerHeight, z] = Item.Grass;
                    }
                }
            }
        }
        public void SetOres()
        {
            var random = new System.Random();
            int OreVeinsNumber = random.Next(6, 10);
            for (int i = 0; i < OreVeinsNumber; i++)
            {
                OreVein oreVein = biomFactory.CreateOreVein();
                chunk.SetOre(oreVein, random.Next(16), random.Next(stoneLayerHeight), random.Next(16));
            }
        }
        public void SetPlants()
        {
            var random = new System.Random();
            int treeNumber = random.Next(1, 2);
            for (int i = 0; i < treeNumber; i++)
            {
                Tree tree = biomFactory.CreateTree();
                int xPos = random.Next(5, 12);
                int yPos = stoneLayerHeight + dirtLayerHeight + grassLayerHeight;
                int zPos = random.Next(5, 12);
                chunk.SetTree(tree, xPos, yPos, zPos);
            }
        }
        public Chunk GetChunk()
        {
            return chunk;
        }
    }

    public class DesertChunkBuilder : ChunkBuilder
    {
        private Chunk chunk = new Chunk();
        BiomComponentsFactory biomFactory = new DesertComponentsFactory();
        private int sandLayerHeight = 4;
        private int stoneLayerHeight = 4;
        public void Reset()
        {
            chunk = new Chunk();
        }
        public void SetGround()
        {
            for (int x = 0; x < 16; x++)
            {
                for (int z = 0; z < 16; z++)
                {
                    for (int y = 0; y < stoneLayerHeight; y++)
                    {
                        chunk.blocks[x, y, z] = Item.Stone;
                    }
                    for (int y = 0; y < sandLayerHeight; y++)
                    {
                        chunk.blocks[x, y + stoneLayerHeight, z] = Item.Sand;
                    }
                }
            }
        }
        public void SetOres()
        {
            var random = new System.Random();
            int OreVeinsNumber = random.Next(4, 7);
            for (int i = 0; i < OreVeinsNumber; i++)
            {
                OreVein oreVein = biomFactory.CreateOreVein();
                chunk.SetOre(oreVein, random.Next(16), random.Next(stoneLayerHeight), random.Next(16));
            }
        }
        public void SetPlants()
        {
            var random = new System.Random();
            int treeNumber = random.Next(1, 3);
            for (int i = 0; i < treeNumber; i++)
            {
                Tree tree = biomFactory.CreateTree();
                int xPos = random.Next(4, 13);
                int yPos = stoneLayerHeight + sandLayerHeight;
                int zPos = random.Next(4, 13);
                chunk.SetTree(tree, xPos, yPos, zPos);
            }
        }
        public Chunk GetChunk()
        {
            return chunk;
        }
    }

    public interface BiomComponentsFactory
    {
        public Tree CreateTree();
        public OreVein CreateOreVein();
    }

    public class ForestComponentsFactory : BiomComponentsFactory
    {
        public Tree CreateTree()
        {
            var random = new System.Random();
            ForestTree tree = new ForestTree();
            int stemHeight = random.Next(5, 8);
            int leavesRadius = tree.leavesRadius = random.Next(2, 4);
            int sizeX = tree.sizeX = leavesRadius * 2 + 1;
            int sizeY = tree.sizeY = stemHeight + leavesRadius;
            int sizeZ = tree.sizeZ = leavesRadius * 2 + 1;
            tree.blocks = new Item[sizeX, sizeY, sizeZ];
            Vector3 center = new Vector3(sizeX / 2, stemHeight, sizeZ / 2);
            for (int x = 0; x < sizeX; x++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    for (int z = 0; z < sizeZ; z++)
                    {
                        Vector3 current = new Vector3(x, y, z);
                        if (Vector3.Distance(center, current) < leavesRadius + 1)
                        {
                            tree.blocks[x, y, z] = Item.OakLeaves;
                        }
                    }
                }
            }
            for (int z = 0; z < stemHeight; z++)
            {
                tree.blocks[sizeX / 2, z, sizeZ / 2] = Item.Oak;
            }
            return tree;
        }
        public OreVein CreateOreVein()
        {
            var random = new System.Random();
            int choice = random.Next(10);
            if (choice < 4)
            {
                return new CoalOreVein() { diameter = 3 };
            }
            else
            {
                return new IronOreVein() { diameter = 2 };
            }
        }
    }

    public class DesertComponentsFactory : BiomComponentsFactory
    {
        public Tree CreateTree()
        {
            var random = new System.Random();
            DesertTree tree = new DesertTree();
            tree.sizeX = 1;
            tree.sizeY = random.Next(4, 7);
            tree.sizeZ = 1;
            tree.blocks = new Item[tree.sizeX, tree.sizeY, tree.sizeZ];
            for (int z = 0; z < tree.sizeY; z++)
            {
                tree.blocks[0, z, 0] = Item.Oak;
            }
            return tree;
        }
        public OreVein CreateOreVein()
        {
            var random = new System.Random();
            int choice = random.Next(10);
            if (choice < 8)
            {
                return new CoalOreVein() { diameter = 2 };
            }
            else
            {
                return new DiamondOreVein() { diameter = 2 };
            }
        }
    }

    public abstract class Tree
    {
        public Item[,,] blocks;
        public int sizeX;
        public int sizeY;
        public int sizeZ;
        public int x;
        public int y;
        public int z;
    }

    public class ForestTree : Tree
    {
        public int leavesRadius;
    }

    public class DesertTree : Tree { }

    public abstract class OreVein
    {
        public int diameter;
        public abstract Item block();
    }

    public class CoalOreVein : OreVein
    {
        public override Item block()
        {
            return Item.CoalOre;
        }
    }

    public class IronOreVein : OreVein
    {
        public override Item block()
        {
            return Item.IronOre;
        }
    }

    public class DiamondOreVein : OreVein
    {
        public override Item block()
        {
            return Item.DiamondOre;
        }
    }
}